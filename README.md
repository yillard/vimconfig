# vim_config

vim configuration files

## vimrc bindings
- CTRL-k move line up
- CTRL-j move line down
- F5 check vhdl syntax  
- F6 compile vhdl simulation  
- F7 show results in gtkwave  