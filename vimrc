set nocompatible
filetype off

set rtp+=$HOME/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'bfrg/vim-cpp-modern'
Plugin 'ayu-theme/ayu-vim'
Plugin 'python-mode/python-mode'
"Plugin 'suoto/vim-hdl'
"Plugin 'chriskempson/base16-vim'
call vundle#end()

filetype plugin indent on

syntax enable
let ayucolor="mirage"
colorscheme ayu
let g:airline_theme='ayu'
let c_no_curly_error=1

set number
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set smartindent
set guifont=Monospace\ 10
if has('gui_running')
	set columns=165
	set lines=44
    set termguicolors
endif


" make winpos dependent on caller, f.e pass arguments to vim from command line
" :echo split( system( "ps -o command= -p " . getpid() ) )
winpos 328 140

set guioptions-=T
set guioptions-=r
set guioptions-=L
set guioptions-=m
set backspace=2
set autochdir

highlight trailingWhitespace ctermbg=gray guibg=gray
match trailingWhitespace /\s\+$/

" move line with CTRL-j, CTRL-k
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <ESC>:m .+1<CR>==gi
inoremap <C-k> <ESC>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv




" if vhdl
autocmd FileType vhdl nnoremap <buffer> <F5> :call GhdlCheck()<CR>
autocmd FileType vhdl nnoremap <buffer> <F6> :call GhdlRun()<CR>
autocmd FileType vhdl nnoremap <buffer> <F7> :call GtkWaveRun()<CR>

"check vhdl with ghdl
function GhdlCheck()
    let ghdl_workdir='ghdl'
    let cmd_line = 'ghdl -s ' .expand('%')
    botright new
    cabbrev <buffer> q q!
    silent execute '$read !' .cmd_line
    setlocal nomodifiable
endfunction

"compile vhdl with ghdl
function GhdlRun()
    let cmd_line_1 = 'ghdl -i --workdir=ghdl *.vhd'
    let cmd_line_2 = 'ghdl -a --workdir=ghdl --ieee=synopsys *.vhd'
    let cmd_line_3 = 'ghdl -e --workdir=ghdl --ieee=synopsys ' .expand('%:r')
    let cmd_line_4 = 'ghdl -r --workdir=ghdl ' .expand('%:r') .' --wave=' .expand('%:r') .'.ghw'
    "let cmd_line_4 = 'ghdl -r --workdir=ghdl ' .expand('%:r') .' --vcd=' .expand('%:r') .'.vcd'
    botright new
    cabbrev <buffer> q q!
    silent execute '$read !' .cmd_line_1
    if !v:shell_error
        silent execute '$read !' .cmd_line_2
    endif
    if !v:shell_error
        silent execute '$read !' .cmd_line_3
    endif
    if !v:shell_error
        silent execute '$read !' .cmd_line_4
    endif
    setlocal nomodifiable
endfunction

function GtkWaveRun()
    let cmd_line_1 = 'gtkwave ' .expand('%:r') .'.ghw &'
    botright new
    cabbrev <buffer> q q!
    silent execute '$read !' .cmd_line_1
    setlocal nomodifiable
endfunction




"Python-mode

"autocmd Filetype python setlocal expandtab tabstop=4 shiftwidth=4
autocmd FileType python nnoremap <buffer> <F6> :call PymodeRun()<CR>

function PymodeRun()
    :PymodeRun
endfunction

" Override go-to.definition key shortcut to Ctrl-]
"let g:pymode_rope_goto_definition_bind = "<C-]>"

" Override run current python file key shortcut to Ctrl-Shift-e
let g:pymode_run_bind = "<C-S-e>"

" Override view python doc key shortcut to Ctrl-Shift-d
let g:pymode_doc_bind = "<C-S-d>"
let g:pymode_python = 'python3'
let g:pymode_run_bind = '<leader>r'
let g:pymode_options_max_line_length = 120
